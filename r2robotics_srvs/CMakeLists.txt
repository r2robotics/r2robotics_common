cmake_minimum_required(VERSION 2.8.3)
project(r2robotics_srvs)

find_package(catkin REQUIRED COMPONENTS
  std_msgs
  std_srvs
  message_generation
  roscpp
  rospy
  r2robotics_msgs
)

 add_service_files(
   FILES
   GetLidarPosition.srv
   GetTableElements.srv
   GetEnnemiesPositions.srv
   AddDisk.srv
   AddRectangle.srv
   GoTo.srv
   Consigne.srv
   GetConsigne.srv
   DoPath.srv
   DoRelativePath.srv
   GoToPoint.srv
   set_servos_pwm.srv
   motors_disable.srv
   motors_control.srv
   motors_speed.srv
   motors_temp.srv
   eep_write.srv
   eep_read.srv
   ram_write.srv
   ram_read.srv
   i_jog.srv
   s_jog.srv
   stat.srv
   rollback.srv
   reboot.srv
   activate.srv
   deactivate.srv
   move.srv
   turn.srv
   move_direction.srv
   move_simultaneous.srv
   turn_simultaneous.srv
   get_position.srv
   clear_errors.srv
   set_position.srv
   set_direction.srv
   set_self_rotation.srv
   set_circle.srv
   get_turrets_robot_frame.srv
   move_turrets_robot_frame.srv
   triggerAvoidance.srv
   testAvoidance.srv
   setControlMaxLinearSpeed.srv
   setControlMaxAngularSpeed.srv
   setControlMinLinearSpeed.srv
   setControlMinAngularSpeed.srv
   stopControl.srv
   resumeControl.srv
   startRecalibration.srv
   stopRecalibration.srv
   setControlTimeout.srv
)

 generate_messages(
   DEPENDENCIES
   std_msgs
   r2robotics_msgs
)


catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES r2robotics_srvs
  CATKIN_DEPENDS std_msgs std_srvs message_runtime r2robotics_msgs
#  DEPENDS system_lib
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

